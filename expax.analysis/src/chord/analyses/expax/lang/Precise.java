package chord.analyses.expax.lang;

public class Precise {
	public static float precise(float f){return f;}
	public static int precise(int i){return i;}
	public static double precise(double d){return d;}
	public static byte precise(byte b){return b;}
	public static char precise(char c){return c;}
	public static short precise(short s){return s;}
	public static long precise(long l){return l;}
	
	//TODO generate enough possible precise_all
	public static double[] precise_all_FIELD1_TAG1(double[] d){return d;}
	public static int[] precise_all_FIELD2_TAG1(int[] arr) {return arr;}
}
