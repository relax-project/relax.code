package chord.analyses.expax.lang;

public class Accept {
	public static boolean accept(boolean b){return b;}
	public static float accept(float f){return f;}
	public static int accept(int i){return i;}
	public static double accept(double d){return d;}
	public static byte accept(byte b){return b;}
	public static char accept(char c){return c;}
	public static short accept(short s){return s;}
	public static long accept(long l){return l;}
	
	//TODO generate enough possible accept_all
	public static int[] accept_all_FIELD2_TAG1(int[] arr) {return arr;}
	public static int[] accept_all_FIELD1_TAG11(int[] arr) {return arr;}
	public static int[] accept_all_FIELD1_TAG2(int[] arr) {return arr;}
	public static int[] accept_all_FIELD1_TAG5(int[] arr) {return arr;}
	public static float[] accept_all_FIELD1_TAG6(float[] arr) {return arr;}
	public static double[] accept_all_FIELD1_TAG1(double[] arr){return arr;}
}
